package am.egs.lessons.service;

import am.egs.lessons.bean.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by haykh on 5/16/2019.
 */
public class PersonCRUDServiceTest {

  private static PersonCRUDService service = new PersonCRUDService();
  @BeforeClass
  public static void initDataForTest(){
    service.createH2DBForTest();
  }

  @Test
  public void addUserTest() {
    Person person = new Person();
    person.setEmail("haykh@hayk.am");
    person.setName("hayk");
    Integer saved_person_id = service.add(person);
    Assert.assertTrue(saved_person_id > 0);

    Person fromDbPerson = service.getById(saved_person_id);
    Assert.assertEquals(saved_person_id, fromDbPerson.getId());
  }

  @Test
  public void updateUserTest() {
    Person person = new Person();
    person.setEmail("haykh@hayk.am");
    person.setName("hayk");
    Integer saved_person_id = service.add(person);
    Assert.assertTrue(saved_person_id > 0);
    Person fromDbPerson = service.getById(saved_person_id);
    fromDbPerson.setName("haykChange");
    service.edit(fromDbPerson);

    Person updateDbPerson = service.getById(saved_person_id);
    Assert.assertEquals("haykChange", updateDbPerson.getName());
  }
}
