package am.egs.lessons.service;

import am.egs.lessons.bean.Person;
import am.egs.lessons.db.ConnectionManager;

import java.sql.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by haykh on 5/16/2019.
 */
public class PersonCRUDService {
  private static final Logger logger =
          LoggerFactory.getLogger(PersonCRUDService.class);

  /**
   *
   */
  private Connection dbConnection = null;

  /**
   * create new user
   * @param person
   * @return
   */
  public int add(Person person) {
    logger.info("PersonCRUDService add user");
    int result = 0;

    Statement statement = null;
    try {
      dbConnection = ConnectionManager.getInstance().getConnection();
      statement = dbConnection.createStatement();
      result = statement.executeUpdate("INSERT INTO person(name, email) VALUES ('"+person.getName()+"', '"+person.getEmail()+"')");
      return result;
    } catch (SQLException e) {
      logger.error("PersonCRUDService e"+e);
    } finally {
      logger.info("PersonCRUDService disconnect user");
      statement = null;
      ConnectionManager.getInstance().disconnect();
    }
    return result;
  }

  /**
   * update person data
   * @param person
   * @return
   */
  public int edit(Person person) {
    logger.info("edit user");
    int result = 0;

    PreparedStatement statement = null;
    try {
      dbConnection = ConnectionManager.getInstance().getConnection();
      statement = dbConnection.prepareStatement("update person set name = ?, email= ? where id = ?");
      statement.setString(1, person.getName());
      statement.setString(2, person.getEmail());
      statement.setInt(3, person.getId());
      statement.executeUpdate();

    } catch (SQLException e) {
      logger.error("edit error:"+e);
    } finally {
      logger.info("disconnect user");
      statement = null;
      ConnectionManager.getInstance().disconnect();
    }
    return result;
  }

  /**
   * get Person by id
   * @param person_id
   * @return
   */
  public Person getById(Integer person_id) {
    logger.info("getById get by id");
    Person person = new Person();
    Statement statement = null;
    try {
      dbConnection = ConnectionManager.getInstance().getConnection();
      statement = dbConnection.createStatement();

      ResultSet rs = statement.executeQuery("select id, name, email from person where id="+person_id+"");
      //ResultSet rs = statement.executeQuery
      while(rs.next()) {
        person.setId(rs.getInt("id"));
        person.setName(rs.getString("name"));
        person.setEmail(rs.getString("email"));
      }
    } catch (SQLException e) {
      logger.error("getById e"+e);
    } finally {
      logger.info("disconnect user");
      statement = null;
      ConnectionManager.getInstance().disconnect();
    }
    return person;
  }

  /**
   * create H2 DB For JUnit Test
   */
  public void createH2DBForTest(){
    logger.info("createDB");
    Statement statement = null;
    try {
      dbConnection = ConnectionManager.getInstance().getConnection();
      statement = dbConnection.createStatement();

      statement.execute("CREATE TABLE person " +
              "(id IDENTITY NOT NULL PRIMARY KEY, " +
              " name VARCHAR(255) NULL, " +
              " email VARCHAR(255) NULL " +
              " ); ");
    } catch (SQLException e) {
      logger.error("createDB e"+e);
    } finally {
      logger.info("createDB disconnect");
      statement = null;
      ConnectionManager.getInstance().disconnect();
    }
  }

}
