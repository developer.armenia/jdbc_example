package am.egs.lessons.bean;

import java.io.Serializable;
//import lombok.Getter;
//import lombok.Setter;
/**
 * Created by haykh on 5/16/2019.
 */
public class Person {
  private Integer id;
  //@Setter @Getter
  private String name;
  //@Setter @Getter
  private String email;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
